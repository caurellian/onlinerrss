package by.denprokazov.onlrss;

import java.util.ArrayList;
import java.util.List;

import by.denprokazov.onlrss.viewModels.FeedItem;
import by.denprokazov.onlrss.xmlModels.onliner.Item;
import by.denprokazov.onlrss.xmlModels.onliner.OnlinerRss;
import by.denprokazov.onlrss.xmlModels.tutby.TutByItem;
import by.denprokazov.onlrss.xmlModels.tutby.TutbyRss;

public class ViewModelFactory {
    public static List<FeedItem> getFeedViewModels(OnlinerRss onlinerRss) {
        List<Item> items = onlinerRss.getChannel().getItems();
        List<FeedItem> feedItems = new ArrayList<>();
        for(int i = 0; i < items.size(); i++) {
            FeedItem feedItem = new FeedItem(
                    onlinerRss.getChannel().getTitle()
                    ,items.get(i).getTitle()
                    ,items.get(i).getPubDate()
                    ,items.get(i).getMediaThumbnail().getUrl()
                    ,items.get(i).getDescription()
                    ,items.get(i).getLink());
            feedItems.add(feedItem);
        }
        return feedItems;
    }

    public static List<FeedItem> getTutByViewModels(TutbyRss tutbyRss) {
        List<TutByItem> items = tutbyRss.getChannel().getItemList();
        List<FeedItem> feedItems = new ArrayList<>();
        for(int i = 0; i < items.size(); i++) {
            FeedItem feedItem = new FeedItem(
                    tutbyRss.getChannel().getTitle()
                    ,items.get(i).getTitle()
                    ,items.get(i).getPubDate()
                    ,items.get(i).getContent().get(0).getUrl()
                    ,items.get(i).getDescription()
                    ,items.get(i).getLink());
            feedItems.add(feedItem);
        }
        return feedItems;

    }
}
