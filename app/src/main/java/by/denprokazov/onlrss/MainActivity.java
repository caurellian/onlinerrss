package by.denprokazov.onlrss;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import by.denprokazov.onlrss.fragments.onliner.AutoFeedsFragment;
import by.denprokazov.onlrss.fragments.onliner.PeopleFeedsFragment;
import by.denprokazov.onlrss.fragments.onliner.RealtFeedsFragment;
import by.denprokazov.onlrss.fragments.onliner.TechFeedsFragment;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_sliding_tab);
        initUI();
        initUIL();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseDB();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initDB();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initDB();
    }

    private void initUIL() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);
    }

    public void initUI() {
        initToolBar("Onliner.by");
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(getSupportFragmentManager(),
                FragmentPagerItems.with(this)
                        .add(R.string.people_onliner_name, PeopleFeedsFragment.class)
                        .add(R.string.auto_onliner_name, AutoFeedsFragment.class)
                        .add(R.string.realt_onliner_name, RealtFeedsFragment.class)
                        .add(R.string.tech_onliner_name, TechFeedsFragment.class)
                        .create());
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);
    }

    protected void initToolBar(String title) {
        Toolbar mainToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        mainToolbar.setTitle(title);
        setSupportActionBar(mainToolbar);

        final String[] sidebarArray = {"Onliner.by", "Tut.by"};
        ArrayAdapter<String> sidebarListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, sidebarArray);

        final DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ListView sideLV = (ListView) findViewById(R.id.side_list_view);
        sideLV.setAdapter(sidebarListAdapter);
        ActionBarDrawerToggle sidebarTooggle = new ActionBarDrawerToggle(
                this, drawerLayout, mainToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        sidebarTooggle.syncState();
        drawerLayout.setDrawerListener(sidebarTooggle);
        sideLV.bringToFront();

        sideLV.bringToFront();
        sideLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String element = sidebarArray[position];
                switch (element) {
                    case "Onliner.by":
                        Intent intent;
                        if (!this.getClass().getSimpleName().equals("MainActivity")) {
                            intent = new Intent(getApplication(), MainActivity.class);
                            startActivity(intent);
                        }
                        break;
                    case "Tut.by":
                        if (!this.getClass().getSimpleName().equals("TutbyActivity")) {
                            intent = new Intent(getApplication(), TutByActivity.class);
                            startActivity(intent);
                        }
                        break;

                }
            }
        });
    }


    private void releaseDB() {
        HelperFactory.releaseHelper();
    }

    private void initDB() {
        HelperFactory.setDatabaseHelper(getApplicationContext());
    }
}
