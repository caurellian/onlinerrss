package by.denprokazov.onlrss.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.denprokazov.onlrss.DetailFeedActivity;
import by.denprokazov.onlrss.HelperFactory;
import by.denprokazov.onlrss.R;
import by.denprokazov.onlrss.adapters.FeedListViewAdapter;
import by.denprokazov.onlrss.api.response.Response;
import by.denprokazov.onlrss.db.DatabaseHelper;
import by.denprokazov.onlrss.loaders.OnlinerLoader;
import by.denprokazov.onlrss.loaders.TutbyLoader;
import by.denprokazov.onlrss.viewModels.FeedItem;

public class FeedsFragment extends Fragment implements OnRefreshListener
        , LoaderManager.LoaderCallbacks<Response> {
    private static final String CATEGORY_NAME = null;
    private int ID;
    private SwipeRefreshLayout feedRefreshLayout;
    private FeedListViewAdapter feedListViewAdapter;
    private List<FeedItem> feedItems = new ArrayList<>();
    private Handler handler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDB();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSwipeLayout(view);
        ListView feedList = (ListView) view.findViewById(R.id.fragment_list_view);
        feedList.setOnItemClickListener(getFeedItemClickListener());
        feedListViewAdapter = new FeedListViewAdapter(this.getActivity(), R.layout.feed_item, feedItems);
        feedList.setAdapter(feedListViewAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(feedItems.size() == 0 && isOnline()) {
            onRefresh();
        } else if(feedItems.size() == 0 && !isOnline()) {
            showNoConnectionToastIfNull(feedItems);
        }
    }

    @NonNull
    private AdapterView.OnItemClickListener getFeedItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), DetailFeedActivity.class);
                intent.putExtra("title", feedItems.get(position).getTitle());
                intent.putExtra("picUrl", feedItems.get(position).getImage());
                intent.putExtra("pubDate", feedItems.get(position).getPubDate());
                intent.putExtra("description", feedItems.get(position).getDescription());
                intent.putExtra("link", feedItems.get(position).getLink());
                startActivity(intent);
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshAdapterFromDB();
    }

    @Override
    public void onRefresh() {
        if(isOnline()) {
            showProgressBar();
        }
        refreshData(ID);
    }

    @Override
    public Loader<Response> onCreateLoader(int id, Bundle args) {
        switch (id) {

            case R.id.auto_onliner_loader:
                return new OnlinerLoader(getActivity().getApplicationContext(), "auto");
            case R.id.people_onliner_loader:
                return new OnlinerLoader(getActivity().getApplicationContext(), "people");
            case R.id.tech_onliner_loader:
                return new OnlinerLoader(getActivity().getApplicationContext(), "tech");
            case R.id.realt_onliner_loader:
                return new OnlinerLoader(getActivity().getApplicationContext(), "realt");
            case R.id.tutbyAuto:
                return new TutbyLoader(getActivity().getApplicationContext(), R.id.tutbyAuto);
            case R.id.tutbyAccidents:
                return new TutbyLoader(getActivity().getApplicationContext(), R.id.tutbyAccidents);
            case R.id.tutbyAfisha:
                return new TutbyLoader(getActivity().getApplicationContext(), R.id.tutbyAfisha);
            case R.id.tutbyCulture:
                return new TutbyLoader(getActivity().getApplicationContext(), R.id.tutbyCulture);
            case R.id.tutbyEconomicsAndBusiness:
                return new TutbyLoader(getActivity().getApplicationContext(), R.id.tutbyEconomicsAndBusiness);
            case R.id.tutbyFinances:
                return new TutbyLoader(getActivity().getApplicationContext(), R.id.tutbyFinances);
            case R.id.tutbyIt:
                return new TutbyLoader(getActivity().getApplicationContext(), R.id.tutbyIt);
            case R.id.tutbyLadies:
                return new TutbyLoader(getActivity().getApplicationContext(), R.id.tutbyLadies);
            case R.id.tutbyPolitics:
                return new TutbyLoader(getActivity().getApplicationContext(), R.id.tutbyPolitics);
            case R.id.tutbyPress:
                return new TutbyLoader(getActivity().getApplicationContext(), R.id.tutbyPress);
            case R.id.tutbyRealty:
                return new TutbyLoader(getActivity().getApplicationContext(), R.id.tutbyRealty);
            case R.id.tutbySociety:
                return new TutbyLoader(getActivity().getApplicationContext(), R.id.tutbySociety);
            case R.id.tutbySports:
                return new TutbyLoader(getActivity().getApplicationContext(), R.id.tutbySports);
            case R.id.tutbyWorldNews:
                return new TutbyLoader(getActivity().getApplicationContext(), R.id.tutbyWorldNews);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Response> loader, Response data) {
        //data from network automatically saves to db
        refreshAdapterFromDB();
        feedListViewAdapter.notifyDataSetChanged();
        stopRefresh();
    }

    private void stopRefresh() {
        feedRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onLoaderReset(Loader<Response> loader) {
    }

    public void refreshData(int id) {
        feedListViewAdapter.clear();
        DatabaseHelper databaseHelper = HelperFactory.getDatabaseHelper();
        if (databaseHelper == null) {
            initDB();
        }

        if(isOnline()) {
            getLoaderManager().restartLoader(id, Bundle.EMPTY, this).forceLoad();
        } else {
            refreshAdapterFromDB();
        }
    }

    public void refreshAdapterFromDB() {
        try {
            feedItems = getDataFromDB(CATEGORY_NAME);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(feedItems.size() == 0 && !isOnline()) {
            showNoConnectionToastIfNull(feedItems);
        }
        feedListViewAdapter.clear();

        feedListViewAdapter.addAll(feedItems);
        feedListViewAdapter.notifyDataSetChanged();
        stopRefresh();
    }


    public List<FeedItem> getDataFromDB(String categoryName) throws SQLException {
        DatabaseHelper databaseHelper = HelperFactory.getDatabaseHelper();
        if (databaseHelper == null) {
            initDB();
        }
        return databaseHelper.getFeedItemDAO().getItemsByCategory(categoryName);
    }

    private void showNoConnectionToastIfNull(List<FeedItem> feedItems) {
        if(feedItems.size() == 0 && !isOnline()) {
            Toast.makeText(getActivity().getApplicationContext(), "Кэш пуст. Пожалуйста проверьте подключение"
                    , Toast.LENGTH_SHORT).show();
            stopRefresh();
        } else if(!isOnline()) {
            Toast.makeText(getActivity().getApplicationContext(), "Соединение отсутствует. Будет загружена локальная копия за " + feedItems.get(0).getPubDate()
                    , Toast.LENGTH_SHORT).show();


        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void initSwipeLayout(View view) {
        feedRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        feedRefreshLayout.setOnRefreshListener(this);
        feedRefreshLayout.setColorSchemeResources(R.color.onlinerRed, R.color.onlinerColorAccent, R.color.onlinerColorPrimary);
    }

    private void initDB() {
        HelperFactory.setDatabaseHelper(getActivity().getApplicationContext());
        try {
            feedItems = getDataFromDB(CATEGORY_NAME);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void showProgressBar() {
        handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                feedRefreshLayout.setRefreshing(true);
            }
        });
    }
}
