package by.denprokazov.onlrss.fragments.tutby;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.sql.SQLException;
import java.util.List;

import by.denprokazov.onlrss.R;
import by.denprokazov.onlrss.api.response.Response;
import by.denprokazov.onlrss.fragments.FeedsFragment;
import by.denprokazov.onlrss.viewModels.FeedItem;

public class LadiesFragment extends FeedsFragment {
    public static final int ID = R.id.tutbyLadies;
    private static final String CATEGORY_NAME = "TUT.BY: Новости ТУТ - Леди";
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void refreshData(int id) {
        super.refreshData(ID);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
    }

    @Override
    public Loader<Response> onCreateLoader(int id, Bundle args) {
        return super.onCreateLoader(id, args);
    }

    @Override
    public void onLoadFinished(Loader<Response> loader, Response data) {
        super.onLoadFinished(loader, data);
    }

    @Override
    public void onLoaderReset(Loader<Response> loader) {
        super.onLoaderReset(loader);
    }


    @Override
    public void refreshAdapterFromDB() {
        super.refreshAdapterFromDB();
    }

    @Override
    public List<FeedItem> getDataFromDB(String categoryName) throws SQLException {
        return super.getDataFromDB(CATEGORY_NAME);
    }

    @Override
    public boolean isOnline() {
        return super.isOnline();
    }

    @Override
    public void initSwipeLayout(View view) {
        super.initSwipeLayout(view);
    }
}
