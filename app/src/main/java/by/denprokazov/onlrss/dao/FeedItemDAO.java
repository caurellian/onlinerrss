package by.denprokazov.onlrss.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import by.denprokazov.onlrss.viewModels.FeedItem;

public class FeedItemDAO  extends BaseDaoImpl<FeedItem, Integer>{
    public FeedItemDAO(ConnectionSource connectionSource, Class<FeedItem> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<FeedItem> getItemsByCategory(String categoryName) throws SQLException {
        QueryBuilder<FeedItem, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("category", categoryName);
        PreparedQuery<FeedItem> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }

    public void deleteRowsByCategoryName(String categoryName) throws SQLException {
        DeleteBuilder<FeedItem, Integer> deleteBuilder = deleteBuilder();
        deleteBuilder.where().eq("category", categoryName);
        deleteBuilder.delete();
    }
}
