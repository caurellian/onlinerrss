package by.denprokazov.onlrss;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class DetailFeedActivity extends AppCompatActivity {

    private String title;
    private String picUrl;
    private String pubDate;
    private String description;
    private String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_feed_activity);
        getExtra();
        initUI();
    }

    public void initUI() {
        initToolBar();

        ImageView feedImage = (ImageView) findViewById(R.id.detail_item_image);
        TextView feedTitle = (TextView) findViewById(R.id.detail_item_title);
        TextView feedDescription = (TextView) findViewById(R.id.detail_item_description);
        if (feedDescription != null) {
            feedDescription.setOnClickListener(getOnDescriptionListener());
        }
        TextView feedPubDate = (TextView) findViewById(R.id.detail_item_pubDate);
        fillViews(feedImage, feedTitle, feedDescription, feedPubDate);
    }

    private void initUIL() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);
    }

    @NonNull
    private View.OnClickListener getOnDescriptionListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(link));
                startActivity(intent);
            }
        };
    }

    private void fillViews(ImageView feedImage, TextView feedTitle, TextView feedDescription, TextView feedPubDate) {
        setImage(feedImage);
        feedTitle.setText(title);
        feedPubDate.setText(pubDate);
        feedDescription.setText(description);
    }

    private void setImage(ImageView feedImage) {
        initUIL();
        ImageLoader.getInstance().displayImage(picUrl,
                feedImage);
    }

    protected void initToolBar() {
        Toolbar mainToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        assert mainToolbar != null;
        mainToolbar.setTitle("");
        setSupportActionBar(mainToolbar);
        mainToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    private void getExtra() {
        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        picUrl = intent.getStringExtra("picUrl");
        pubDate = intent.getStringExtra("pubDate");
        description = intent.getStringExtra("description");
        link = intent.getStringExtra("link");
    }
}
