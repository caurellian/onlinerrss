package by.denprokazov.onlrss.viewModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "FeedItems")
public class FeedItem {

    @DatabaseField(generatedId = true)
    private int Id;
    @DatabaseField
    private String title;
    @DatabaseField
    private String pubDate;
    @DatabaseField
    private String image;
    @DatabaseField
    private String description;
    @DatabaseField
    private String category;

    @DatabaseField
    private String link;

    public FeedItem(String category, String title, String pubDate, String image, String description, String link) {
        this.category = category;
        this.title = title;
        this.pubDate = pubDate;
        this.image = image;
        this.description = description;
        this.link = link;
    }

    public String getCategory() {
        return category;
    }


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public FeedItem() {
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
