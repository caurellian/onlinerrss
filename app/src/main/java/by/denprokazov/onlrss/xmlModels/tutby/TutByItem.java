package by.denprokazov.onlrss.xmlModels.tutby;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.convert.Convert;
import org.simpleframework.xml.convert.Converter;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.OutputNode;

import java.util.List;


@Root(name = "item", strict = false)
public class TutByItem {
    @Element(name = "title")
    private String title;

    @Element(name = "link")
    private String link;

    @Element(name = "pubDate")
    private String pubDate;

    @Element(name = "category")
    private String category;

    @Element(name = "guid")
    private Guid guid;

    @ElementList(inline = true, name = "content", required = false)
    private List<Content> content;

    @Element(data = true, name = "description", required = false)
    @Convert(DescriptionEmptyHandler.class)
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public Guid getGuid() {
        return guid;
    }

    public void setGuid(Guid guid) {
        this.guid = guid;
    }

    public List<Content> getContent() {
        return content;
    }

    public void setContent(List<Content> content) {
        this.content = content;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public class DescriptionEmptyHandler implements Converter<String> {


        @Override
        public String read(InputNode node) throws Exception {
            String value = node.getValue();
            if(value == null) {
                value = "Читать полную версию";
            }
            return value;
        }

        @Override
        public void write(OutputNode node, String value) throws Exception {
            node.setValue(value);
        }

    }
}
