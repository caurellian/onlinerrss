package by.denprokazov.onlrss.xmlModels.tutby;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class Image {
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Element
    private String url;
    @Element
    private String title;
    @Element
    private String link;
}
