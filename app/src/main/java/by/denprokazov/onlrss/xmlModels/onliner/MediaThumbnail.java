package by.denprokazov.onlrss.xmlModels.onliner;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root
public class MediaThumbnail {
    @Attribute(name = "url")
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
