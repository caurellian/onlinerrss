package by.denprokazov.onlrss.xmlModels.tutby;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name = "guid")
public class Guid {
    public boolean isPermaLink() {
        return isPermaLink;
    }

    public void setIsPermaLink(boolean isPermaLink) {
        this.isPermaLink = isPermaLink;
    }

    public String getGuidLink() {
        return guidLink;
    }

    public void setGuidLink(String guidLink) {
        this.guidLink = guidLink;
    }

    @Attribute(name = "isPermaLink")
    private boolean isPermaLink;

    @Text
    private String guidLink;
}
