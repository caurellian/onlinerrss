package by.denprokazov.onlrss.xmlModels.onliner;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class Image {
    @Element(name = "url")
    private String url;

    @Element(name = "width")
    private String width;

    @Element(name = "height")
    private String height;

    @Element(name = "title")
    private String title;

    @Element(name = "link")
    private String link;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
