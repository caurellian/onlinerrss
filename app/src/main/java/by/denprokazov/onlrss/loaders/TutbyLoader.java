package by.denprokazov.onlrss.loaders;

import android.content.Context;

import java.io.IOException;

import by.denprokazov.onlrss.R;
import by.denprokazov.onlrss.api.ApiFactory;
import by.denprokazov.onlrss.api.TutbyService;
import by.denprokazov.onlrss.api.response.RequestResult;
import by.denprokazov.onlrss.api.response.Response;
import by.denprokazov.onlrss.api.response.TutByResponse;
import by.denprokazov.onlrss.xmlModels.tutby.TutbyRss;
import retrofit2.Call;

public class TutbyLoader extends SimpleAsyncLoader {
    private final Context context;
    private final int id;
    private TutbyRss tutbyRss;

    public TutbyLoader(Context context, int id) {
        super(context);
        this.context = context;
        this.id = id;
    }

    @Override
    protected void onError() {

    }

    @Override
    protected void onSuccess() {

    }

    @Override
    protected Response apiCall() throws IOException {
        TutbyService tutbyService = ApiFactory.getTutbyService();
        Call<TutbyRss> call = null;
        switch (id) {
            case R.id.tutbyPolitics:
                call = tutbyService.getPolitics();
                break;
            case R.id.tutbyEconomicsAndBusiness:
                call = tutbyService.getEconomicsAndBusiness();
                break;
            case R.id.tutbySociety:
                call = tutbyService.getSociety();
                break;
            case R.id.tutbyWorldNews:
                call = tutbyService.getWorldNews();
                break;
            case R.id.tutbyCulture:
                call = tutbyService.getCulture();
                break;
            case R.id.tutbyAccidents:
                call = tutbyService.getAccidents();
                break;
            case R.id.tutbyFinances:
                call = tutbyService.getFinances();
                break;
            case R.id.tutbyRealty:
                call = tutbyService.getRealty();
                break;
            case R.id.tutbyAuto:
                call = tutbyService.getAuto();
                break;
            case R.id.tutbySports:
                call = tutbyService.getSports();
                break;
            case R.id.tutbyLadies:
                call = tutbyService.getLadies();
                break;
            case R.id.tutbyIt:
                call = tutbyService.getIt();
                break;
            case R.id.tutbyAfisha:
                call = tutbyService.getAfisha();
                break;
            case R.id.tutbyPress:
                call = tutbyService.getPress();
                break;
            default:
                call = null;
                break;
        }

        if (call != null) {
            tutbyRss = call.execute().body();
        }

        TutByResponse tutByResponse = (TutByResponse) new TutByResponse()
                .setRequestResult(RequestResult.SUCCESS)
                .setAnswer(tutbyRss);
        tutByResponse.save(context);
        return tutByResponse;
    }
}
