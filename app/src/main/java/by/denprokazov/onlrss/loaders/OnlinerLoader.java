package by.denprokazov.onlrss.loaders;

import android.content.Context;

import java.io.IOException;

import by.denprokazov.onlrss.api.ApiFactory;
import by.denprokazov.onlrss.api.OnlinerService;
import by.denprokazov.onlrss.api.response.OnlinerResponse;
import by.denprokazov.onlrss.api.response.RequestResult;
import by.denprokazov.onlrss.api.response.Response;
import by.denprokazov.onlrss.xmlModels.onliner.OnlinerRss;
import retrofit2.Call;

public class OnlinerLoader extends SimpleAsyncLoader {
    private final String thirdDomain;
    private final Context context;

    public OnlinerLoader(Context context, String thirdDomain) {
        super(context);
        this.thirdDomain = thirdDomain;
        this.context = context;
    }

    @Override
    protected Response apiCall() throws IOException {
        OnlinerService onlinerService = ApiFactory.getOnlinerService(thirdDomain);
        Call<OnlinerRss> call = onlinerService.getFeed();
        OnlinerRss onlinerRss = call.execute().body();
        OnlinerResponse onlinerResponse = (OnlinerResponse) new OnlinerResponse()
                .setRequestResult(RequestResult.SUCCESS)
                .setAnswer(onlinerRss);
        onlinerResponse.save(context);
        return onlinerResponse;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
    }

    @Override
    protected void onError() {
    }

    @Override
    protected void onSuccess() {
    }
}
