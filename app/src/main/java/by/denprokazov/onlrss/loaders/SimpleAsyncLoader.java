package by.denprokazov.onlrss.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import java.io.IOException;

import by.denprokazov.onlrss.api.response.RequestResult;
import by.denprokazov.onlrss.api.response.Response;

public abstract class SimpleAsyncLoader extends AsyncTaskLoader {

    public SimpleAsyncLoader(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public Response loadInBackground() {
        try {
            Response response = apiCall();
            if(response.getRequestResult() == RequestResult.SUCCESS) {
                response.save(getContext());
                onSuccess();
            } else {
                onError();
            }
            return response;
        } catch (IOException e) {
            e.printStackTrace();
            onError();
            return new Response();
        }
    }

    protected abstract void onError();

    protected abstract void onSuccess();


    protected abstract Response apiCall() throws IOException;
}
