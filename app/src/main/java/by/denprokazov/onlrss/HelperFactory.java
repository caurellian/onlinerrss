package by.denprokazov.onlrss;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import by.denprokazov.onlrss.db.DatabaseHelper;

public class HelperFactory {
    private static DatabaseHelper databaseHelper;

    public static DatabaseHelper getDatabaseHelper() {
        return databaseHelper;
    }

    public static void setDatabaseHelper(Context applicationContext) {
        databaseHelper = OpenHelperManager.getHelper(applicationContext, DatabaseHelper.class);
        databaseHelper.getWritableDatabase();
    }

    public static void releaseHelper() {
        OpenHelperManager.releaseHelper();
        databaseHelper = null;
    }
}
