package by.denprokazov.onlrss.api;

import by.denprokazov.onlrss.xmlModels.tutby.TutbyRss;
import retrofit2.Call;
import retrofit2.http.GET;

public interface TutbyService {
    @GET("politics.rss")
    Call<TutbyRss> getPolitics();
    @GET("economics.rss")
    Call<TutbyRss> getEconomicsAndBusiness();
    @GET("society.rss")
    Call<TutbyRss> getSociety();
    @GET("world.rss")
    Call<TutbyRss> getWorldNews();
    @GET("culture.rss")
    Call<TutbyRss> getCulture();
    @GET("accidents.rss")
    Call<TutbyRss> getAccidents();
    @GET("finance.rss")
    Call<TutbyRss> getFinances();
    @GET("realty.rss")
    Call<TutbyRss> getRealty();
    @GET("auto.rss")
    Call<TutbyRss> getAuto();
    @GET("sport.rss")
    Call<TutbyRss> getSports();
    @GET("lady.rss")
    Call<TutbyRss> getLadies();
    @GET("it.rss")
    Call<TutbyRss> getIt();
    @GET("afisha.rss")
    Call<TutbyRss> getAfisha();
    @GET("press.rss")
    Call<TutbyRss> getPress();
}
