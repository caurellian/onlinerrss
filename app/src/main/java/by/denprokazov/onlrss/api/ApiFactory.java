package by.denprokazov.onlrss.api;

import android.support.annotation.NonNull;

import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class ApiFactory {


    public static OnlinerService getOnlinerService(String thirdDomain) {
        return getOnlinerRetrofit(thirdDomain).create(OnlinerService.class);
    }

    public static TutbyService getTutbyService() {
        return getTutbyRetrofit().create(TutbyService.class);
    }

    @NonNull
    private static Retrofit getOnlinerRetrofit(String thirdDomain) {
        return new Retrofit.Builder()
                .baseUrl("https://" + thirdDomain + ".onliner.by")
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
    }

    @NonNull
    private static Retrofit getTutbyRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://news.tut.by/rss/")
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
    }


}
