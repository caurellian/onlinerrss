package by.denprokazov.onlrss.api;

import by.denprokazov.onlrss.xmlModels.onliner.OnlinerRss;
import retrofit2.Call;
import retrofit2.http.GET;

public interface OnlinerService {

    @GET("/feed")
    Call<OnlinerRss> getFeed();
}
