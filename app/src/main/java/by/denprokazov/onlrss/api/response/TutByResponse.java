package by.denprokazov.onlrss.api.response;

import android.content.Context;

import org.jsoup.Jsoup;
import org.jsoup.parser.Parser;
import org.jsoup.safety.Whitelist;

import java.sql.SQLException;
import java.util.List;

import by.denprokazov.onlrss.HelperFactory;
import by.denprokazov.onlrss.ViewModelFactory;
import by.denprokazov.onlrss.db.DatabaseHelper;
import by.denprokazov.onlrss.viewModels.FeedItem;
import by.denprokazov.onlrss.xmlModels.tutby.TutbyRss;

public class TutByResponse extends Response {

    @Override
    public void save(Context context) {
        super.save(context);
        TutbyRss tutbyRss = getTypedAnswer();
        if (tutbyRss != null) {
            if(HelperFactory.getDatabaseHelper() != null) {
                try {
                    HelperFactory.getDatabaseHelper().getFeedItemDAO().deleteRowsByCategoryName(tutbyRss.getChannel().getTitle());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        if (tutbyRss != null) {
            DatabaseHelper databaseHelper = HelperFactory.getDatabaseHelper();
            if(databaseHelper == null) {
                initDB(context);
            }

            List<FeedItem> feedItemList = ViewModelFactory.getTutByViewModels(tutbyRss);
            for (FeedItem feedItem : feedItemList) {
                if (databaseHelper != null) {

                    //clean title from encodings
                    String title  = feedItem.getTitle();
                    title = Parser.unescapeEntities(title,false);
                    feedItem.setTitle(title);

                    //clean description from tags and encodings
                    String description = feedItem.getDescription();
                    description = Jsoup.clean(description, Whitelist.none());
                    description = Parser.unescapeEntities(description, false);
                    feedItem.setDescription(description);
                    try {
                        databaseHelper.getFeedItemDAO().create(feedItem);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    private void initDB(Context context) {
        HelperFactory.setDatabaseHelper(context);
    }
}
