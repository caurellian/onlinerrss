package by.denprokazov.onlrss.api.response;

public enum RequestResult {
    SUCCESS,
    ERROR
}
