package by.denprokazov.onlrss;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import by.denprokazov.onlrss.fragments.tutby.AccidentsFragment;
import by.denprokazov.onlrss.fragments.tutby.AfishasFragment;
import by.denprokazov.onlrss.fragments.tutby.AutoFragment;
import by.denprokazov.onlrss.fragments.tutby.CultureFragment;
import by.denprokazov.onlrss.fragments.tutby.EconomicsAndBusinessFragment;
import by.denprokazov.onlrss.fragments.tutby.FinancesFragment;
import by.denprokazov.onlrss.fragments.tutby.ItFragment;
import by.denprokazov.onlrss.fragments.tutby.LadiesFragment;
import by.denprokazov.onlrss.fragments.tutby.PoliticsFragment;
import by.denprokazov.onlrss.fragments.tutby.RealtyFragment;
import by.denprokazov.onlrss.fragments.tutby.SocietyFragment;
import by.denprokazov.onlrss.fragments.tutby.SportsFragment;
import by.denprokazov.onlrss.fragments.tutby.WorldNewsFragment;

public class TutByActivity extends MainActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void initUI() {
        initToolBar("Tut.by");
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(getSupportFragmentManager(),
                FragmentPagerItems.with(this)
                        .add(R.string.accidents_tutby_name, AccidentsFragment.class)
                        .add(R.string.afisha_tutby_name, AfishasFragment.class)
                        .add(R.string.auto_tutby_name, AutoFragment.class)
                        .add(R.string.culture_tutby_name, CultureFragment.class)
                        .add(R.string.economics_tutby_name, EconomicsAndBusinessFragment.class)
                        .add(R.string.finances_tutby_name, FinancesFragment.class)
                        .add(R.string.it_tutby_name, ItFragment.class)
                        .add(R.string.ladies_tutby_name, LadiesFragment.class)
                        .add(R.string.politics_tutby_name, PoliticsFragment.class)
                        .add(R.string.realty_tutby_name, RealtyFragment.class)
                        .add(R.string.society_tutby_name, SocietyFragment.class)
                        .add(R.string.sport_tutby_name, SportsFragment.class)
                        .add(R.string.world_tutby_name, WorldNewsFragment.class)
                        .create());

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);
    }

    @Override
    protected void initToolBar(String title) {
        super.initToolBar("Tut.by");
    }
}
