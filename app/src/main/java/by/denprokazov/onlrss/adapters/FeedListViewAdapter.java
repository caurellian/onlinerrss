package by.denprokazov.onlrss.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;


import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import by.denprokazov.onlrss.R;
import by.denprokazov.onlrss.viewModels.FeedItem;

public class FeedListViewAdapter extends ArrayAdapter<FeedItem> {
    private final List<FeedItem> feedItems;
    private final Context context;
    private DisplayImageOptions displayImageOptions;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    public FeedListViewAdapter(Context context, int resource, List<FeedItem> feedItems) {
        super(context, resource, feedItems);
        this.feedItems = feedItems;
        this.context = context;
        configUIL();
    }

    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }

    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public FeedItem getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View view = convertView;

        if(view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.feed_item, null, true);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.feed_item_title);
            viewHolder.icon = (ImageView) view.findViewById(R.id.feed_item_image);
            viewHolder.date = (TextView) view.findViewById(R.id.feed_item_date);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.title.setText(feedItems.get(position).getTitle());
        viewHolder.date.setText(feedItems.get(position).getPubDate());
        getImage(position, viewHolder);
        return view;
    }

    static class ViewHolder {

        ImageView icon;
        TextView title;
        TextView date;
    }

    private void configUIL() {
        displayImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_pic)
                .showImageForEmptyUri(R.drawable.no_pic)
                .showImageOnFail(R.drawable.no_pic)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    private void getImage(int position, ViewHolder viewHolder) {
        ImageLoader.getInstance().displayImage(feedItems.get(position).getImage(),
                viewHolder.icon,
                displayImageOptions, animateFirstListener);
    }

    private void initUIL() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);
    }
}